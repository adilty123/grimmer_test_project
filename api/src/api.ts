import operations from './controllers/getUsers'
import Users from './models/users'

import express, { request, response } from 'express';
import bodyParser from 'body-parser';
import cors from 'cors';

var app = express();
var router = express.Router();

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(cors());
app.use('/api', router);

router.use((request,response,next) =>{
    console.log('middleware');
    next();
});

//http://localhost:8090/api/allusers
router.route('/allusers').get((request,response)=>{
    operations.getUsers().then((result: any) => {
        console.log(result);
        console.log(response.statusCode)
        response.json(result);
    })
    .catch((error) => {
        console.error(error.message);
    });
});

//http://localhost:8090/api/deleteuser/:id
router.route('/deleteuser/:id').delete((request,response)=>{
    operations.deleteUser(request.params.id).then((result: any) => {
        if(response.status(200)){
            console.log(result);
            response.json('Successfully deleted user. ID =' + request.params.id);
        }
    })
    .catch((error) => {
        console.error(error.message);
    });
});

//http://localhost:8090/api/adduser
router.route('/adduser').post((request,response)=>{
    let user = {...request.body}

    operations.addUser(user).then((result: any) => {
        //console.log(result);
        response.status(201).json("Successfully added user. " + result);
    })
    .catch((error) => {
        console.error(error.message);
    });
});


var port = 8090
app.listen(port);
console.log('API running on: ' + port)

/*
get().then((result: any) => {
    console.log(result);
});
*/