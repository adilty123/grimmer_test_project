import sql from 'mssql';
import config from "../dbconfig";

async function getUsers(){
    try{
        let pool = await sql.connect(config);
        let users = await pool.request().query("SELECT * from Users");
        return users.recordsets;
    }
    catch(error){
        console.log(error);
    }
}

async function deleteUser(userId:string){
    try{
        let pool = await sql.connect(config);
        let deleteUser = await pool.request()
            .input('input_parameter', sql.Int, userId)
            .query("DELETE FROM Users WHERE Id = @input_parameter");
        return deleteUser.recordsets;
    }
    catch(error){
        console.log(error);
    }
}

async function addUser(user: any){
    try{
        let pool = await sql.connect(config);
        let insertUser = await pool.request()
            .input('username_input', sql.VarChar, user.Username)
            .input('email_input', sql.VarChar, user.Email)
            .query("INSERT INTO Users (Username, Email) VALUES (@username_input, @email_input)");
        return insertUser.recordsets;
    }
    catch(error){
        console.log(error);
    }
}

export default {
    getUsers,
    deleteUser,
    addUser
};