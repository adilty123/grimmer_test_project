const config = {
    server: 'localhost',
    user: 'test',
    password: 'login',
    database: 'TestUsers',
    options:{
        trustedconnection: true,
        enableArithAbort: true,
    }
}

export default config;