import React, { useEffect, useState } from "react";
import "./styles.css";
import "./pagination.css";
import { User } from "../user";
import fetchUsers from "../requests/GetUsers";
import SingleUser from "./SingleUser";
import ReactPaginate from "react-paginate";


const UserList: React.FC = () =>{
    const [data, setData] = useState<User[]>();
    const [currentPage, setCurrentPage] = useState(0);
    const [totalPages, setTotalPages] = useState(0);
    const itemsPerPage = 20;

    useEffect (() => {
        fetchUsers().then(function(response) {
            const items = response.map(u => u)
            console.log(items)
            setData(items)
            setTotalPages(Math.ceil (items.length/ itemsPerPage));
        });
    }, []);

    const startIndex = currentPage * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const subset = data?.slice(startIndex, endIndex);

    const handlePageChange = (selectedPage:any) => {
        setCurrentPage(selectedPage.selected);
      };
    return (
    <div className="user">
        {subset?.map((user) => (
            <SingleUser user = {user} key = {user.Id} data = {data} setData = {setData}/>
        ))}
        <br/>
        <div className="pagination_container">
        <ReactPaginate
            activeClassName={'item active '}
            breakClassName={'item break-me '}
            breakLabel={'...'}
            containerClassName={'pagination'}
            disabledClassName={'disabled-page'}
            marginPagesDisplayed={2}
            nextClassName={"item next "}
            nextLabel={">>>"}
            onPageChange={handlePageChange}
            forcePage={currentPage}
            pageCount={totalPages}
            pageClassName={'item pagination-page '}
            pageRangeDisplayed={2}
            previousClassName={"item previous"}
            previousLabel={"<<<"}
        />
        </div>

    </div>

    )
}

export default UserList;