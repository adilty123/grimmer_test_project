import React from 'react';
import { User } from '../user';
import { MdDelete } from 'react-icons/md';
import "./styles.css";
import DeleteUser from '../requests/DeleteUser';

interface Props{
    user: User,
    data: User[] | undefined,
    setData: React.Dispatch<React.SetStateAction<User[] | undefined>>
}

const SingleUser: React.FC<Props> = ({user, data, setData}: Props) =>{
    
    const handleDelete = (id:number) =>{
            DeleteUser(id).then(res => {
                if(res.includes('Successfully')){
                    setData(data?.filter((user) => user.Id !== id));
                }
            })
    }
    
    return(
        <form className='user_single'>
            <span className='user_single-text'>
                {user.Username}
            </span>
            <span className='user_single-email'>
                {user.Email}
            </span>
            <div>
                <span className='icon' onClick={()=> handleDelete(user.Id)}>
                    <MdDelete />
                </span>
            </div>
        </form>
    )
}

export default SingleUser