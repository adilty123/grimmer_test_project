import React from "react";
import './styles.css';

interface Props{
    Username: string
    Email: string
    setUser: React.Dispatch<React.SetStateAction<string>>;
    setEmail: React.Dispatch<React.SetStateAction<string>>;
    addHandle: (e: React.FormEvent) => void;
}

const AddUser: React.FC<Props> = ({Username, Email, setUser, setEmail, addHandle}) =>{
    


    return (
        <form className = "input" onSubmit={addHandle}>
            <input value={Username} onChange={(e)=>setUser(e.target.value)} type = "input" placeholder="Enter Username" className="input_box"></input>
            <input value={Email} onChange={(e)=>setEmail(e.target.value)} type = "input" placeholder="Enter Email" className="input_box"></input>
            <button type = "submit" className = "input_submit">Add User</button>
        </form>
    )
}

export default AddUser