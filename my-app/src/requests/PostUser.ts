async function PostUser(username: string, email: string) {
    const headers: Headers = new Headers();
    
    console.log("POSTED: " + username)
    console.log("POSTED: " + email)
    
    var jsonData = {
        Username: username,
        Email: email,
    }

    console.log(jsonData)
    console.log(JSON.stringify(jsonData))

    headers.set('Content-Type', 'application/json')
    headers.set('Accept', 'application/json')
    headers.set('X-Custom-Header', 'CustomValue')

    const request: RequestInfo = new Request('http://localhost:8090/api/adduser/', {
        method: 'POST',
        headers: headers,
        body: JSON.stringify(jsonData)
    })

    return fetch(request)
        .then(res => res.json())
        .then(res => {
            return res
        });
}

export default PostUser;