import { User } from '../user';


//Interface to help store users.

async function fetchUsers(): Promise<User[]>{

    const headers: Headers = new Headers();

    headers.set('Content-Type', 'application/json')
    headers.set('Accept', 'application/json')
    headers.set('X-Custom-Header', 'CustomValue')

    const request: RequestInfo = new Request('http://localhost:8090/api/allusers', {
        method: 'GET',
        headers: headers
    })

    var re: unknown;
    return fetch(request)
        .then(res => res.json())
        .then(res => {
            re = res[0]
            return re as User[]
        });

}


export default fetchUsers;