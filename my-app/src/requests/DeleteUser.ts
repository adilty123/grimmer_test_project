async function DeleteUser(id:number): Promise<string> {
    const headers: Headers = new Headers();

    headers.set('Content-Type', 'application/json')
    headers.set('Accept', 'application/json')
    headers.set('X-Custom-Header', 'CustomValue')

    const request: RequestInfo = new Request('http://localhost:8090/api/deleteuser/'+id.toString(), {
        method: 'DELETE',
        headers: headers
    })

    return fetch(request)
        .then(res => res.json())
        .then(res => {
            return res
        });
}

export default DeleteUser;