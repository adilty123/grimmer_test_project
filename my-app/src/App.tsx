import React, { useState } from 'react';
import './App.css';
import AddUser from './components/AddUser';
import UserList from './components/UserList';
import PostUser from './requests/PostUser';

const App: React.FC = () => {
  const [username, setUser] = useState<string>("");
  const [email, setEmail] = useState<string>("");
  console.log(username, email);

  const addHandle = (e: React.FormEvent) =>{
    PostUser(username, email)
  };
  
  return (
    <div className="App">
      <span className = "heading">Users</span>
      <AddUser Username={username} setUser={setUser} setEmail={setEmail} Email={email} addHandle = {addHandle}/>
      <UserList />
    </div>
  );
}

export default App;
